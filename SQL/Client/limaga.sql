﻿-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :


SET 
SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

SET time_zone = "+00:00";



/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;

/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;

/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;

/*!40101 SET NAMES utf8 *

/;

--
-- Base de données :  `limaga`
--

-- --------------------------------------------------------

--

-- Structure de la table `client`
--


CREATE TABLE IF NOT EXISTS `client` 
(
  `idClient` int(11) NOT NULL AUTO_INCREMENT,

  `login` varchar(32) NOT NULL,

  `mdp` varchar(32) NOT NULL,

  `nom` varchar(32) NOT NULL,

  `prenom` varchar(32) NOT NULL,

  `mail` varchar(64) NOT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB  
DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;



--
-- Contenu de la table `client`
--


INSERT INTO `client` (`id`, `login`, `mdp`, `nom`, `prenom`, `mail`)
 VALUES
(1, 'Bond7', 'mission', 'Bond', 'Jame', 'kisslondon@gmail.com'),

(2, 'rocky2', 'ko', 'Balboa', 'Rocky', 'kickyourass@gmail.com'),

(3, 'Einstein', 'cqfd', 'Einstein', 'Albert', 'ilovescience@gmail.com'),

(4, 'swn5', 'winteriscomming', 'Snow', 'jon', 'wallwinter@gmail.com'),

(5, 'StarkArrya', 'valarmorghulis', 'Stark', 'Arrya', 'winteriscoming@gmail');



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;

/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
