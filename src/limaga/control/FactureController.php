<?php

namespace limaga\control;

use limaga\models\LeconCollective;
use limaga\models\LeconIndividuelle;
use limaga\vue\VueFacture;

class FactureController extends AbstractController {
  public function merciBillet($id){
    $vue = new VueFacture(array($id));
    $vue->render(1);

    $billet = \limaga\models\EBillet::find($id);
    $billet -> paye = true;
    $billet->save();
  }

  public function factureBillet($id){
    $vue = new VueFacture(array($id));
    $vue->render(2);
  }

 public function merciBilletLecon($id){
    $vue = new VueFacture(array($id));
    $vue->render(3);

    $billet = \limaga\models\EBillet::find($id);
    $billet -> paye = true;
    $billet->save();
  }

public function factureBilletLecon($id){
    $vue = new VueFacture(array($id));
    $vue->render(4);        
  }

  public function facturePanier($id){
    $vue = new VueFacture(array($id));
    $vue->render(5);
  }
}
