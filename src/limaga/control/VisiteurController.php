<?php

namespace limaga\control;

use Illuminate\Database\QueryException;
use limage\vue\VueVisiteur;

class VisiteurController extends AbstractController {
	public function inscription(){
		$vue = new \limaga\vue\VueVisiteur();
		$vue->render(1);
	}

	public function auth(){
		$vue = new \limaga\vue\VueVisiteur();
		$vue->render(2);
	}

	public function inscrire(){
		$p = $this->request->post();
		$c = new \limaga\models\Client();

		$c->prenom = filter_var($p['prenom'], FILTER_SANITIZE_STRING);
		$c->nom= filter_var($p['nom'], FILTER_SANITIZE_STRING);
		$c->login = filter_var($p['user'], FILTER_SANITIZE_STRING);
		$c->mdp = password_hash(filter_var($p['mdp'], FILTER_SANITIZE_STRING), PASSWORD_BCRYPT);
		$c->mail = filter_var($p['email'], FILTER_SANITIZE_EMAIL);
        $c->nvNatation = filter_var($p['nvNatation'], FILTER_SANITIZE_STRING);
        $c->estAdmin = 0;
		$c->save();

        $app = \Slim\Slim::getInstance();
        try{
            $mail = new \PHPMailer();
            $body = "Bonjour $c->prenom $c->nom ! Vous venez de vous inscrire sur le site de Limaga
            avec le login \"$c->login\".<br/>Ceci est un mail de confirmation, merci de pas y répondre ";

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 's3bislimaga@gmail.com';                 // SMTP username
            $mail->Password = 'limagas3bis';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;

            $mail->SetFrom('s3bislimaga@gmail.com', 'limaga limaga');
            $mail->AddReplyTo('s3bislimaga@gmail.com', 'limaga limaga');

            $mail->Subject    = "limaga mail de confirmation";

            $mail->MsgHTML($body);

            $address = $c->mail;

            $mail->AddAddress($address, "$c->nom $c->prenom" );

            if(!$mail->Send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                echo "Message sent!";
            }
            $c->save();


        }catch ( QueryException $e){
            $app->redirect($app->urlFor("base"));
        }
		$app->redirect($app->urlFor("base"));
	}


	public function authentifier(){
		$p = $this->request->post();
		$u = \limaga\models\Client::where('login', '=', $p['user']) -> get() -> first();

		if(isset($u)){
			if(password_verify($p['mdp'], $u->mdp)){
                if($u->estAdmin){
                    session_start();
                    
                    $_SESSION['username'] = $u->login;
                    $_SESSION['userid'] = $u->idClient;
                    $_SESSION['level'] = 1;
                    $_SESSION['admin'] = 1;
                    $app = \Slim\Slim::getInstance();
                    $app->redirect($app->urlFor("admin", array("id" => $u->idClient)));
                }else {
                    session_start();
                    
                    $_SESSION['username'] = $u->login;
                    $_SESSION['userid'] = $u->idClient;
                    $_SESSION['level'] = 1;
                    $app = \Slim\Slim::getInstance();
                    $app->redirect($app->urlFor("client", array("id" => $u->idClient)));
                }
            }
			else{
				$app = \Slim\Slim::getInstance();
				$app->redirect($app->urlFor("auth"));
			}
		}
		else{
			$app = \Slim\Slim::getInstance();
			$app->redirect($app->urlFor("auth"));
		}
	}

	public function afficherPanier(){
		$vue = new \limaga\vue\VueVisiteur();
		$vue -> render(3);
	}

	public function ajouterProduitPanier($idProduit){
		session_start();

		if(empty($_SESSION['panier'])){
			$_SESSION['panier'] = array();
		}

		array_push($_SESSION['panier'], \limaga\models\Materiel::find($idProduit));

		$app = \Slim\Slim::getInstance();

		$app->redirect($app->urlFor("catalogue"));
	}

    public function ajouterProduitLouerPanier($idProduit){
        session_start();

        if(empty($_SESSION['panier'])){
            $_SESSION['panier'] = array();
        }

        $produit = \limaga\models\Materiel::find($idProduit);
        $produit -> estLouer = true;
        $prix =  $produit->prix;
        $produit->prix = $prix/2.0;
        array_push($_SESSION['panier'], $produit);

        $app = \Slim\Slim::getInstance();

        $app->redirect($app->urlFor("catalogue"));
    }

	public function supprimerProduitPanier($idProduit){
		session_start();

		$produit_a_sup = \limaga\models\Materiel::find($idProduit);


		foreach ($_SESSION['panier'] as $index => $produit) {
			if($produit->idProduit == $produit_a_sup->idProduit){
				unset($_SESSION['panier'][$index]);
				break;
			}
		}


		$app = \Slim\Slim::getInstance();
        $produit = \limaga\models\Materiel::find($idProduit);
        $produit ->estLouer = 0;
        $produit ->save();

		$app->redirect($app->urlFor("panier"));
	}

	public function destroySession(){
		session_start();
		session_destroy();

		$app = \Slim\Slim::getInstance();
		$app->redirect($app->urlFor("base"));
	}

	public function afficherCatalogue(){
		$vue = new \limaga\vue\VueVisiteur();
		$vue -> render(4);
	}

    public function pageaccueil(){
        $vue = new \limaga\vue\VueVisiteur();
        $vue -> render(0);
    }

}
