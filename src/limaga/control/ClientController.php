<?php

namespace limaga\control;

use limaga\models\LeconCollective;
use limaga\models\LeconIndividuelle;
use limaga\models\EBillet;
use limaga\vue\VueClient;

class ClientController extends AbstractController {
	public function afficherClient($id){
		session_start();
		if($_SESSION['level'] >= 1){
			$vue = new VueClient(\limaga\models\Client::find($id));
			$vue->render(1);
		}
		else{
			$app = \Slim\Slim::getInstance();
			$app->redirect($app->urlFor("auth"));
		}
	}

	public function afficherAdmin($id){
		session_start();
		if($_SESSION['level'] >= 1){
			$vue = new VueClient(\limaga\models\Client::find($id));
			$vue->render(15);
		}
		else{
			$app = \Slim\Slim::getInstance();
			$app->redirect($app->urlFor("auth"));
		}
	}

	public function deconnexion(){
		session_start();
		unset($_SESSION['userid']);

		$app = \Slim\Slim::getInstance();
        $app->redirect($app->urlFor("base"));
	}

	public function ajouterMembre(){
		session_start();
		if($_SESSION['level'] >= 1){
			$vue = new VueClient(null);
			$vue->render(2);
		}
		else{
			$app = \Slim\Slim::getInstance();
			$app->redirect($app->urlFor("auth"));
		}

	}

    public function suppMembre($id){
        session_start();
        $u = \limaga\models\Client::where('login', '=', $_SESSION['username']) -> get() -> first();

        $_SESSION['username'] = $u->login;
        $_SESSION['userid'] = $u->idClient;
        $_SESSION['level'] = 1;
        $m = MembreFamille::find($id);
        if((!is_null($m))){
            $m->delete();
        }
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->urlFor("client", array("id" => $u->idClient)));
    }


	public function insererMembre(){
		$p = $this->request->post();
		$c = new \limaga\models\MembreFamille();

		session_start();

		$c->idClient = $_SESSION['userid'];
		$c->prenom = filter_var($p['prenom'], FILTER_SANITIZE_STRING);
		$c->nom= filter_var($p['nom'], FILTER_SANITIZE_STRING);
		$c->nvxNatation= filter_var($p['niveau'], FILTER_SANITIZE_NUMBER_INT);

		$c->save();

		$app = \Slim\Slim::getInstance();
		$app->redirect($app->urlFor("client", array('id' => $_SESSION['userid'] )));
	}

	public function afficherAbonnements(){
    session_start();
		$vue = new VueClient(null);
		$vue->render(4);
	}

    public function commandeLecon(){
        session_start();
        if($_SESSION['level'] >= 1){
            //$lecons = 'collective' => \limaga\models\LeconCollective::where('nbPlace', '>', 0);
            $vue = new VueClient(null);
			$vue->render(3);
		}
		else{
			$app = \Slim\Slim::getInstance();
			$app->redirect($app->urlFor("auth"));
		}
    }

    public function commanderLecon(){
        session_start();
        $p = $this->request->post();
        if($p['select'] == "collective"){
            $lecons = \limaga\models\LeconCollective::where('nbPlace', '>', 0) -> get();
            //$lecons = \limaga\models\LeconCollective::all();
            $vue = new VueClient($lecons);
            $vue->render(8);
        } else if ($p['select'] == "individuelle") {
            $vue = new VueClient(null);
            $vue->render(9);         
        }        
    }

    public function LeconIndividuelleCommandee(){
        session_start();
        $p = $this->request->post();
        
        $lecon = new \limaga\models\LeconIndividuelle();
        $lecon->dateLeconInd = $p['date'];
        $lecon->idClient = $_SESSION['userid'];
        $lecon->nbLeconInd = $p['nbLecons'];
        $lecon->save();

        $billet = new \limaga\models\EBillet();
        $billet->idClient = $_SESSION['userid'];
        $billet->dateBillet = $p['date'];
        $billet->typeAccesBillet = 0;
        $billet->paye = false;
        $billet->codeBarreBillet = rand(0,99999999);
        $billet->save();

        $app = \Slim\Slim::getInstance();
        $app->redirect($app->urlFor("choixpaiementLecon", array('id' => $billet->idBillet)));
    }

    public function LeconCollectiveCommandee(){
        $p = $this->request->post();
        $lecon = \limaga\models\LeconCollective::where('dateLeconCol', '=', $p['select']) -> get();
        $lecon[0]->nbPlace -= 1;
        $lecon[0]->save();
        
    }

    public function achatBillet(){
      session_start();
    	$b = new \limaga\vue\VueClient(null);
    	$b->render(5);
    }

    public function enregistrerBillet(){
    	session_start();
    	$app = \Slim\Slim::getInstance();
    	$p = $this->request->post();

    	if($p['type'] == "matin" || $p['type'] == "aprem"){
    		$billets = \limaga\models\EBillet::where('typeAccesBillet', '=', $p['type'], 'and', 'dateBillet', '=', filter_var($p['date'], FILTER_SANITIZE_STRING));
        $billets_jour = \limaga\models\EBillet::where('dateBillet', '=', filter_var($p['date'], FILTER_SANITIZE_STRING), 'and', 'typeAccessBillet', '=', 'jour');

   			if(count($billets) + count($billets_jour)> 100){
   				$vue = new VueClient(null);
   				$vue->render(6);
   			}
   			else{
   				$b = new \limaga\models\EBillet();

   				$b->idClient = $_SESSION['userid'];
   				$b->dateBillet = filter_var($p['date'], FILTER_SANITIZE_STRING);
   				if( $p['type'] == "matin" ){
   					$b->typeAccesBillet = 1;
   				}
   				else if($p['type'] == "aprem"){
   					$b->typeAccesBillet = 2;
   				}
          $b->paye = false;
          $b->codeBarreBillet = rand(0,99999999);

          $b->save();

          $app = \Slim\Slim::getInstance();
          $app->redirect($app->urlFor("choixpaiement", array('id' => $b->idBillet)));
   			}
    	}
      else{
        $billets_matin = \limaga\models\EBillet::where('typeAccesBillet', '=', "matin", 'and', 'dateBillet', '=', filter_var($p['date'], FILTER_SANITIZE_STRING));
        $billets_soir = \limaga\models\EBillet::where('typeAccesBillet', '=', "aprem", 'and', 'dateBillet', '=', filter_var($p['date'], FILTER_SANITIZE_STRING));
        $billets_jour = \limaga\models\EBillet::where('typeAccesBillet', '=', "jour", 'and', 'dateBillet', '=', filter_var($p['date'], FILTER_SANITIZE_STRING));

        if(count($billets_matin) + count($billets_soir) + count($billets_jour) > 100){
          $vue = new VueClient(null);
          $vue->render(6);
        }
        else{
          $b = new \limaga\models\EBillet();

          $b->idClient = $_SESSION['userid'];
          $b->dateBillet = filter_var($p['date'], FILTER_SANITIZE_STRING);
          $b->typeAccesBillet = 3;
          $b->paye = false;
          $b->codeBarreBillet = rand(0, 99999999);

          $b->save();

          $app = \Slim\Slim::getInstance();
          $app->redirect($app->urlFor("choixpaiement", array('id' => $b->idBillet)));
        }
      }
    }

    public function souscrireAbo(){
    	session_start();

    	$a = new \limaga\models\EAbonnement();

    	$a->idClient = $_SESSION['userid'];
    	$a->nbEntreAbo = 10;
    }

    public function choixPaiement($id){
      session_start();
      $vue = new VueClient(array($id));
      $vue->render(7);
    }

    public function choixPaiementLecon($id){
      session_start();
      $vue = new VueClient(array($id));
      $vue->render(11);          
    }


    public function impayes($id){
      session_start();
      if($_SESSION['level'] >= 1){
        $billets = EBillet::where('idClient', '=', $id)->get();
        $vue = new VueClient($billets);
        $vue->render(10);
      }
      else{
        $app = \Slim\Slim::getInstance();
        $app->redirect($app->urlFor("auth"));
      }
    }

    public function afficheCaisse(){
            session_start();
        $u = \limaga\models\Client::where('login', '=', $_SESSION['username']) -> get() -> first();
        $p = $this->request->post();
        $vue = new VueClient($p);
        $vue->render(12);

    }

    
    public function controlleCaisse(){
        session_start();
        $u = \limaga\models\Client::where('login', '=', $_SESSION['username']) -> get() -> first();

        $p = $this->request->post();
        $code = \limaga\models\EBillet::find($p['code']);
        if(isset($code)){
            $code->delete();
            $vue = new VueClient($p['code']);
            $vue->render(14);            
        }else{
            $vue = new VueClient($p['code']);
            $vue->render(13);            
        }
        
    }

    
}
