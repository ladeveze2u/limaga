<?php

namespace limaga\models;

class EAbonnement extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'eabonnement';
    protected $primaryKey = 'idAbonnement';
    public $timestamps = false;

    public function codeClient(){
    	return $this->belongsTo('\limaga\models\client', 'codeClient');
    }

}