<?php

namespace limaga\models;

class LeconCollective extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'leconCollective';
    protected $primaryKey = 'idLeconCol';
    public $timestamps = false;

    public function client(){
    	return $this->hasMany('\limaga\models\client', 'idClient');
    }

}