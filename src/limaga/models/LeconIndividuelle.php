<?php

namespace limaga\models;

class LeconIndividuelle extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'leconIndividuel';
    protected $primaryKey = 'idLeconInd';
    public $timestamps = false;

    public function client(){
    	return $this->belongsTo('\limaga\models\client', 'idClient');
    }

}