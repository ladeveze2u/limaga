<?php

namespace limaga\models;

class CommandeLecon extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'commandeLecon';
    protected $primaryKey = 'codeClient';
    public $timestamps = false;

}