<?php

namespace limaga\models;

class EBillet extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'ebillet';
    protected $primaryKey = 'idBillet';
    public $timestamps = false;

    public function codeClient(){
    	return $this->belongsTo('\limaga\models\client', 'codeClient');
    }

}