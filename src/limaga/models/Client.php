<?php

namespace limaga\models;

class Client extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'client';
    protected $primaryKey = 'idClient';
    public $timestamps = false;

}