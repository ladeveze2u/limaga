<?php

namespace limaga\models;

class EAbonnementFamille extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'eabonnementFamille';
    protected $primaryKey = 'idAbonnementFamille';
    public $timestamps = false;

    public function codeClient(){
    	return $this->belongsTo('\limaga\models\client', 'codeClient');
    }

}