<?php

namespace limaga\models;

class CommandeBillet extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'commandeBillet';
    protected $primaryKey = 'idCommandeBillet';
    public $timestamps = false;

}