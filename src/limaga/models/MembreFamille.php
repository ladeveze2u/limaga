<?php

namespace limaga\models;

class MembreFamille extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'membreFamille';
    protected $primaryKey = 'idMembre';
    public $timestamps = false;


    function client(){
    	return $this->belongsTo('\limaga\models\client', 'idClient');
    }
}