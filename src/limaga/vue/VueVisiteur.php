<?php

namespace limaga\vue;

use limaga\models\Materiel;

class VueVisiteur{


	public function render($i){
        session_start();

        switch($i){
            case 0:
            $content = $this->accueil();
            break;
            case 1:
            $content = $this->renderFormulaire();
            break;
            case 2:
            $content = $this->renderAuth();
            break;
            case 3:
            $content = $this->renderPanier();
            break;
            case 4:
            $content = $this->renderCatalogue();
            break;
        }



        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();

        $path_auth = $app->urlFor("auth");
        $path_panier = $app->urlFor("panier");
        $path_insc = $app->urlFor("insc");
        $path_deco = $app->urlFor("deco");
        $path_cat = $app->urlFor("catalogue");
        $path_caisse = $app->urlFor("affichecaisse");
        $path_leconCommande = $app->urlFor("commandeLecon");

        if(isset($_SESSION['userid'])){
            if(isset($_SESSION['admin'])){
                if($_SESSION['admin']){
                    $userid = $_SESSION['userid'];
                    $path_client = $app->urlFor('admin', array('id' => $userid));
                    $line = "<a href=\"$path_client\">Mon profil</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a><a href=\"$path_caisse\">Caisse</a><a href=\"$path_leconCommande\">Lecons</a><a href=\"$path_deco\">Deconnexion</a>";
                    
                }
            }
            else {
                
                $userid = $_SESSION['userid'];
                $path_client = $app->urlFor("client", array('id' => $userid));
                
                $line = "<a href=\"$path_client\">Mon profil</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a><a href=\"$path_leconCommande\">Lecons</a><a href=\"$path_deco\">Deconnexion</a>";
            }
        }
        else{
            $line = "<a href=\"$path_auth\">Authentification</a><a href=\"$path_insc\">Inscription</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a>";

        }

        echo <<<END
        <!DOCTYPE html>
        <html>
        <head>
            <title>Limaga</title>
            <meta http-equiv=Content-Type content="text/html; charset=utf-8" >
            <link rel="stylesheet" type="text/css" href="images/app.css"/>
        </head>
        <body>
            <nav>
                <ul>
                 <a href="/limaga">Acceuil</a>
                 $line
             </ul>
         </nav>

         <h1>LIMAGA</h1><br/><br/><br/>

         $content

     </body>
     </html>
END;
 }

 public function renderFormulaire(){
    $app = \Slim\Slim::getInstance();
    $path = $app->request->getRootUri();

    $path_insc = $app->urlFor("insc");

    $result= <<<END
    <form action="$path_insc" method="post">

        <label for="login">Login :</label>
        <input type="text" name="user" required/><br/>

        <label for="mdp">Mot de passe:</label>
        <input type="password" name="mdp" required/><br/>

        <label for="nom">Nom :</label>
        <input type="text" name="nom" required/><br/>

        <label for="prenom">Prenom :</label>
        <input type="text" name="prenom" required/><br/>

        <label for="email">Email :</label>
        <input type="text" name="email" required/><br/>

        <label for="nvNatation">Niveaux Natation :</label>
        <input type="text" name="nvNatation" required/><br/>

        <div class="button">
            <button type="submit">Valider</button>
        </div>

    </form>
END;

    return $result;
}

public function renderPanier(){
    //print_r($_SESSION);
    $prixPanier = 0;
    if(empty($_SESSION['panier'])){
        $result = "<div><b>Votre panier est vide</b></div>";
    }
    else{
        $a = \Slim\Slim::getInstance();
        $path_supprimerAll = $a->urlFor("ds");
        $result = "<div><h4> Panier :</h4><br/><a href=\"$path_supprimerAll\"> Supprimer Panier </a><br/>";

        foreach ($_SESSION['panier'] as $produit) {
            $app = \Slim\Slim::getInstance();


            $idProduit = $produit->idProduit;
            $nom = $produit->nomProduit;
            $prix = $produit->prix;
            $path_supprimer = $app->urlFor("supprimerPanier", array("idProduit" => $idProduit));

            $prixPanier = $prix + $prixPanier;

            $result .= "<p>$nom </br>Cout : $prix </p><a href=\"$path_supprimer\"> Supprimer </a><br/>";
        }

        $result .= "Prix Panier : $prixPanier</div>";
    }

    return $result;
}

public function renderAuth(){
    $app = \Slim\Slim::getInstance();
    $path = $app->request->getRootUri();

    $path_auth = $app->urlFor("auth");

    $result = <<<END
    <form action="$path_auth" method="post">

        <label for="login">Login :</label>
        <input type="text" name="user" /><br/>

        <label for="mdp">Mot de passe:</label>
        <input type="password" name="mdp"/><br/>

        <div class="button">
            <button type="submit">Connexion</button>
        </div>

    </form>

END;

    return $result;
}

public function renderCatalogue(){
    $articles = \limaga\models\Materiel::all();

    $result = "<div><h4> Catalogue :</h4><br/>";
    $app = \Slim\Slim::getInstance();


    $arcType0 =  \limaga\models\Materiel::where('typeProduit', '=', 0)->get();
    $arcType1 = Materiel::where('typeProduit', '=',1)->get();
    $arcType2 = Materiel::where('typeProduit', '=',2)->get();
    $arcType3 = Materiel::where('typeProduit', '=',3)->get();
    $arcType4 = Materiel::where('typeProduit', '=',4)->get();

    $result .="</br><h4>Produit : Tee-Shirt</h4></br>";

    foreach ($arcType0 as $art) {

        $id = $art->idProduit;
        $nom = $art->nomProduit;
        $prix = $art->prix;
        $prixlocation = $prix/2;
        $path_acheter = $app->urlFor("ajouterPanier", array("idProduit" => $id));
        $path_louer = $app->urlFor("ajouterLouerPanier", array("idProduit" => $id));


        $result .= "<p><h5>-$nom</h5> </br>Cout Achat: $prix e </br>Cout Location: $prixlocation e </p></br><a href =\"$path_louer\"> > Louer < </a><br/><a href =\"$path_acheter\"> > Acheter < </a><br/></br>";

    }
    $result .="</br><h4>Produit : Survetement</h4></br>";
    foreach ($arcType1 as $art) {

        $id = $art->idProduit;
        $nom = $art->nomProduit;
        $prix = $art->prix;
        $prixlocation = $prix/2;
        $path_acheter = $app->urlFor("ajouterPanier", array("idProduit" => $id));
        $path_louer = $app->urlFor("ajouterLouerPanier", array("idProduit" => $id));

        $result .= "<p><h5>-$nom</h5> </br>Cout Achat: $prix e </br>Cout Location: $prixlocation e </p><br/><a href =\"$path_louer\"> > Louer < </a><br/><a href =\"$path_acheter\"> > Acheter < </a><br/>";

    }
    $result .="</br><h4>Produit : Short</h4></br>";
    foreach ($arcType2 as $art) {

        $id = $art->idProduit;
        $nom = $art->nomProduit;
        $prix = $art->prix;
        $prixlocation = $prix/2;
        $path_acheter = $app->urlFor("ajouterPanier", array("idProduit" => $id));
        $path_louer = $app->urlFor("ajouterLouerPanier", array("idProduit" => $id));

        $result .= "<p><h5>-$nom</h5></br>Cout Achat: $prix e </br>Cout Location: $prixlocation e </p><br/><a href =\"$path_louer\"> > Louer < </a><br/><a href =\"$path_acheter\"> > Acheter < </a><br/>";

    }
    $result .="</br><h4>Produit : Lunette</h4></br>";
    foreach ($arcType3 as $art) {

        $id = $art->idProduit;
        $nom = $art->nomProduit;
        $prix = $art->prix;
        $prixlocation = $prix/2;
        $path_acheter = $app->urlFor("ajouterPanier", array("idProduit" => $id));
        $path_louer = $app->urlFor("ajouterLouerPanier", array("idProduit" => $id));

        $result .= "<p><h5>-$nom</h5> </br>Cout Achat: $prix e </br>Cout Location: $prixlocation e </p><br/><a href =\"$path_louer\"> > Louer < </a><a href =\"$path_acheter\"> > Acheter < </a><br/>";

    }

    $result .="</br><h4>Produit : Palme</h4></br>";
    foreach ($arcType4 as $art) {

        $id = $art->idProduit;
        $nom = $art->nomProduit;
        $prix = $art->prix;
        $prixlocation = $prix/2;
        $path_acheter = $app->urlFor("ajouterPanier", array("idProduit" => $id));
        $path_louer = $app->urlFor("ajouterPanier", array("idProduit" => $id));

        $result .= "<p><h5>-$nom</h5> </br>Cout Achat: $prix e </br>Cout Location: $prixlocation e </p><br/><a href =\"$path_louer\"> > Louer < </a><br/><a href =\"$path_acheter\"> > Acheter < </a><br/>";

    }


    $result .= "</div>";
    return $result;
}

public function accueil(){
    $app = \Slim\Slim::getInstance();
    $path_cat = $app->urlFor("catalogue");

    $result = <<<END
    
    <div class=\"plate\">
        <h1 align="center">Bienvenue</h1>
        <h1 align="center">Limaga</h1></br>
        </br>
       <h3>Descriptif :</h3>
    </div>
END;

    return $result;
}
}

