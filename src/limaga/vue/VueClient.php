<?php

namespace limaga\vue;

class VueClient{
    private $values;

    public function __construct($array){
        $this->values = $array;
    }

	public function render($i){
        switch($i){
            case 1:
				$content = $this->renderClient();
				break;
            case 2:
				$content = $this->formAjoutMembre();
				break;
            case 3:
				$content = $this->formCommandeLecon();
				break;
            case 4:
				$content = $this->renderAbos();
				break;
            case 5:
				$content = $this->renderAchatBillet();
				break;
            case 6:
				$content = "<div><b>Il n'y pas plus de place disponibles pour ce créneau</b></div>";
				break;
            case 7:
				$content = $this->renderChoixPaiement();
				break;
			case 8:
				$content = $this->renderLeconCollective();
				break;
			case 9:
				$content = $this->renderLeconIndividuelle();
				break;
            case 10:
				$content = $this->renderImpayes();
				break;
            case 11:
				$content = $this->renderChoixPaiementLecon();
				break;
            case 12:
				$content = $this->renderCaisse();
				break;
            case 13:
				$content = $this->renderErreur();
				break;
            case 14:
				$content = $this->renderCodeSupprimé();
				break;
        case 15:
            $content = $this->renderAdmin();
            break;

        }

        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();
        $path_auth = $app->urlFor("auth");
        $path_panier = $app->urlFor("panier");
        $path_insc = $app->urlFor("insc");
        $path_deco = $app->urlFor("deco");
        $path_cat = $app->urlFor("catalogue");
        $path_caisse = $app->urlFor("affichecaisse");
        $path_leconCommande = $app->urlFor("commandeLecon");


        if(isset($_SESSION['userid'])){
            if(isset($_SESSION['admin'])){
                if($_SESSION['admin']){
                    $userid = $_SESSION['userid'];
                    $path_client = $app->urlFor('admin', array('id' => $userid));
                    $line = "<a href=\"$path_client\">Mon profil</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a><a href=\"$path_caisse\">Caisse</a><a href=\"$path_leconCommande\">Lecons</a><a href=\"$path_deco\">Deconnexion</a>";
                    
                }
            }
            else {
                $userid = $_SESSION['userid'];
                $path_client = $app->urlFor('client', array('id' => $userid));
                $line = "<a href=\"$path_client\">Mon profil</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a><a href=\"$path_leconCommande\">Lecons</a><a href=\"$path_deco\">Deconnexion</a>";
                }
        }
        else{
            $line =  "<a href=\"$path_auth\">Authentification</a><a href=\"$path_insc\">Inscription</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a>";

        }

        echo <<<END
<!DOCTYPE html>
<html>
    <head>
        <title>Limaga</title>
        <meta http-equiv=Content-Type content="text/html; charset=utf-8" >
        <link rel="stylesheet" type="text/css" href="$path/images/app.css"/>
    </head>
    <body>
        <nav>
            <ul>
                <a href="/limaga">Acceuil</a>
                $line
            </ul>
        </nav>

        <h1>LIMAGA</h1><br/>

        $content

    </body>
</html>
END;
    }

    public function renderClient(){
        $c = $this->values;

        $app = \Slim\Slim::getInstance();
        $path_famille = $app->urlFor("famille");
        $path_abonnements = $app->urlFor("abonnements");
        $path_billet = $app->urlFor("billet");
        $path_impayes = $app->urlFor("impayes", array("id" => $c->idClient));
        $path_facture = $app->urlFor("facturepanier", array("id" => $c->idClient));

        $result = <<<END
        <div class=\"plate\">


END;

        $result .= "";



        $result .= "</br><h1>Profil :</h1></br><h4>Données :</h4></br>".
			"<div class=\"plate\">
            <p class=\"script\"><p>Nom: ".$c->nom."</p>".
            " <p class=\"script\"><p>Prenom: ".$c->prenom.
            " <p class=\"script\"><p>Nv Natation : ".$c->nvNatation;

        $result .= "</br></br><h4>Action :</h4></br></br><a href = \"$path_billet\"> > Acheter un e-billet < </a><br/>";


        $result .= "</p><a href=\"$path_famille\"> >Ajouter un membre de la famille< </a></br></br><a href=\"$path_abonnements\"> >Souscrire a un abonnement< </br></a><br/><a href=\"$path_impayes\"></br>>Impayés< </a></br></br>
        <a href=\"$path_facture\"> > Payer votre panier < </br></a>";

        $membres = \limaga\models\MembreFamille::where('idClient', '=', $c->idClient) -> get();

        $result .= "</br>
            <table>
                <caption>Membres de la famille :</caption></br>

                <thead>
                   <tr>
                        <th>Nom :</th>
                        <th>Prenom :</th>
                        <th>Niveau de natation:</th>
                    </tr>
                </thead>";

        foreach ($membres as $membre) {
            $path_suppMembre = $app->urlFor("suppMembre",array("id" => $membre->idMembre));
            $nom = $membre->nom;
            $prenom = $membre->prenom;
            $niv = $membre->nvxNatation;

            $result .= "<body>
                                <tr>
                                    <td>$nom</td>
                                    <td>$prenom</td>
                                    <td>$niv</td>
                                     <td><a href='$path_suppMembre'>Supprimer</td>
                                </tr>
                       ";
        }

        $result .= " </body>
            </table></div>";

        return $result;
    }

    public function formAjoutMembre(){
        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();

        $path_famille = $app->urlFor("famille");

        $result = <<<END
<form action="$path_famille" method="post">
        <label for="nom">Nom :</label>
        <input type="text" name="nom" required/><br/>

        <label for="prenom">Prenom :</label>
        <input type="text" name="prenom" required/><br/>

        <label for="niveau">Niveau de natation :</label>
        <input type="text" name="niveau" required/><br/>

        <div class="button">
            <button type="submit">Valider</button>
        </div>
    </form>
END;
        return $result;
    }

    public function formCommandeLecon(){
        $app = \Slim\Slim::getInstance();
        $path_commande = $app->urlFor("commandeLecon");

        $result = <<<END
        <form action="$path_commande" method="post">
        <label for="niveau">Choisir un type de lecon : </label>
        <select name='select'>
            <option value="individuelle">Individuelle</option>
            <option value="collective">Collective</option>        
         </select>
         <input type='submit' name='submit'/>
     </form>
END;
        return $result;
    }

    public function renderAbos(){
        $app = \Slim\Slim::getInstance();

        $path_abo = $app->urlFor("eabo");
        $path_abofamille = $app->urlFor("eabofamille");

        $result = <<<END
<div>
<h4> e-abonnements </h4>

<p> e-abonnement simple <p>
<a href="$path_abo"> Souscrire </a>
<br/>

<p> e-abonnement famille <p>
<a href="$path_abofamille"> Souscrire </a>

</div>
END;

		return $result;
    }

    public function renderAchatBillet(){
        $app = \Slim\Slim::getInstance();

        $path_billet = $app->urlFor("billet");

        $result = <<<END
<div>

<form action="$path_billet" method="post">

        <p>Selectionnez une date pour votre billet : </p><br/>
        <input type="date" name="date" /><br/>

        <p>Selectionnez un type de billet : </p><br/>
        Matin <input type="radio" name="type" value="matin"><br/>
        Apres-midi <input type="radio" name="type" value="aprem"><br/>
        Journee <input type="radio" name="type" value="jour"><br/>

        <div class="button">
            <button type="submit">Connexion</button>
        </div>

    </form>
</div>
END;

		return $result;
    }

	public function renderLeconCollective(){
        $c = $this->values;
        $app = \Slim\Slim::getInstance();
		$path_lecon = $app->urlFor("leconCollective");
		$result = <<<END
        <form action="$path_lecon" method="post">
        <label for="niveau">Choisissez une lecon : </label>
        <select name='select'>
END;

		foreach($c as $lecon){
			$result .= <<<END
            <option value="$lecon->dateLeconCol">$lecon->dateLeconCol</option>
END;
			
		}
		
        $result .= <<<END
         </select>
         <input type='submit' name='submit'/>
     </form>
END;
		return $result;
	}
	
	public function renderLeconIndividuelle(){
        $app = \Slim\Slim::getInstance();
		$path_lecon = $app->urlFor("leconIndividuelle");
		$result = <<<END
   <div>
        <form action="$path_lecon" method="post">
            <p>Selectionnez une date pour votre leçon : </p><br/>
            <input type="date" name="date" /><br/>
            <p>Sélectionnez le nombre de leçons voulues : </p>
            <input type = "number" name = "nbLecons" required/>
            <input type='submit' name='submit'/>

         </form>
   </div>
END;
		return $result;
	}

	public function renderChoixPaiement(){
        $app = \Slim\Slim::getInstance();

        $path_merci = $app->urlFor("mercibillet", array("id" => $this->values[0]));
        $path_facture = $app->urlFor("facturebillet", array("id" => $this->values[0]));

        $result = <<<END
<div>

<a href=$path_merci> Paiement immediat </a><br/>
<a href=$path_facture> Paiement differe </a><br/>

</div>

END;
		
        return $result;
	}

    
	public function renderChoixPaiementLecon(){
        $app = \Slim\Slim::getInstance();

        $path_merci = $app->urlFor("mercibilletLecon", array("id" => $this->values[0]));
        $path_facture = $app->urlFor("facturebilletLecon", array("id" => $this->values[0]));

        $result = <<<END
<div>

<a href=$path_merci> Paiement immediat </a><br/>
<a href=$path_facture> Paiement differe </a><br/>

</div>

END;
		
        return $result;
	}

    public function renderImpayes(){
        $result = "<div>";
        $app = \Slim\Slim::getInstance();

        foreach ($this->values as $impaye) {
            $num = $impaye->idBillet;
            $paye = $impaye->paye;
            $path_payer = $app->urlFor("mercibillet", array("id" => $num));

            if(!$paye){
                $result .= "Billet n° $num&nbsp&nbsp<a href=\"$path_payer\">Payer</a>";
            }
        }
        $result .= "</div>";

        return $result;
    }

    public function renderCaisse(){
        $app = \Slim\Slim::getInstance();
        $path_caisse = $app->urlFor("caisse");
		$result = <<<END
   <div>
        <form action="$path_caisse" method="post">
            <p>Code barre : </p>
            <input type = "number" name = "code" required/>
            <input type='submit' name='submit'/>

         </form>
   </div>
END;
		return $result;
    }

    function renderErreur(){
        $app = \Slim\Slim::getInstance();
		$result = <<<END
ERREUR ! Le code $this->values n'existe pas dans la base de données
END;
		return $result;

    }

    function renderCodeSupprimé(){
        $app = \Slim\Slim::getInstance();
		//$path_lecon = $app->urlFor("leconIndividuelle");
		$result = <<<END
Le code $this->values a été suprimé de la base de données
END;
		return $result;

    }

    public function renderAdmin(){
        $c = $this->values;
        $app = \Slim\Slim::getInstance();
        $path_caisse = $app->urlFor("affichecaisse");
        $result = <<<END
        <div class=\"plate\">
</br><h1>Profil :</h1></br><h4>Données :</h4></br>
			<div class=\"plate\">
            <p class=\"script\"><p>Nom: $c->nom</p>
             <p class=\"script\"><p>Prenom: "$c->prenom
END;
        $result .= "</br></br><h4>Action :</h4></br></br><a href = \"$path_caisse\"> > Gerer la caisse < </a><br/>";
  
        return $result;
    }

}
