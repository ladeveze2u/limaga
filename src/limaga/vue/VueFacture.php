<?php

namespace limaga\vue;

class VueFacture{
    private $values;

    public function __construct($array){
        $this->values = $array;
    }

    public function render($i){
        session_start();
        switch($i){
            case 1:
            $content = $this->renderMerciBillet();
            break;
            case 2:
            $content = $this->renderFactureBillet();
            case 3:
            $content = $this->renderMerciBilletLecon();
            break;
            case 4:
				$content = $this->renderFactureBilletLecon();
				break;
            case 5:
            $content = $this->renderFacturePanier();
            break;
        }

        $app = \Slim\Slim::getInstance();
        $path = $app->request->getRootUri();
        $path_auth = $app->urlFor("auth");
        $path_panier = $app->urlFor("panier");
        $path_insc = $app->urlFor("insc");
        $path_deco = $app->urlFor("deco");
        $path_cat = $app->urlFor("catalogue");
        $path_caisse = $app->urlFor("affichecaisse");
        $path_leconCommande = $app->urlFor("commandeLecon");


        if(isset($_SESSION['userid'])){
            if(isset($_SESSION['admin'])){
                if($_SESSION['admin']){
                    $userid = $_SESSION['userid'];
                    $path_client = $app->urlFor('admin', array('id' => $userid));
                    $line = "<a href=\"$path_client\">Mon profil</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a><a href=\"$path_caisse\">Caisse</a><a href=\"$path_leconCommande\">Lecons</a><a href=\"$path_deco\">Deconnexion</a>";
                    
                }
            }
            else {

            $userid = $_SESSION['userid'];
            $path_client = $app->urlFor('client', array('id' => $userid));
            $line = "<a href=\"$path_client\">Mon profil</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a><a href=\"$path_deco\">Deconnexion</a>";
            }
        }
        else{
            $line =  "<a href=\"$path_auth\">Authentification</a><a href=\"$path_insc\">Inscription</a><a href=\"$path_cat\">Catalogue</a><a href=\"$path_panier\">Panier</a>";

        }

        echo <<<END
        <!DOCTYPE html>
        <html>
        <head>
            <title>Limaga</title>
            <link rel="stylesheet" type="text/css" href="$path/images/app.css"/>
        </head>
        <body>
            <nav>
                <ul>
                    <a href="/limaga">Acceuil</a>
                    $line
                </ul>
            </nav>

            <h1>LIMAGA</h1><br/>

            $content

        </body>
        </html>
END;
    }
    
    public function renderMerciBillet(){
        $billet = \limaga\models\EBillet::find($this->values[0]);

        $idClient = $billet->idClient;
        $num = $this->values[0];
        $client = \limaga\models\Client::find($idClient);
        $nom = $client->nom;
        $prenom = $client->prenom;
        $date = $billet->dateBillet;
        $codeBarre = $billet->codeBarreBillet;

        if($billet->typeAccesBillet == 1){
            $type = "un matin";
        }
        else if($billet->typeAccesBillet == 2){
            $type = "une après-midi";
        }
        else{
            $type = "une journée";
        }

        $result = <<<END

        <div>

            <b>Merci pour votre paiement</b><br/><br/>


            N° de e-billet : $num<br/>
            <br/>
            Nom : $nom<br/>
            Prénom : $prenom<br/>
            <br/>
            Billet d'entrée pour $type, le $date<br/>
            Ce billet vous permettra d'obtenir un bracelet grâce à ce code-barre :<br/>
            <br/>
            <b>$codeBarre</b>
        </p>

    </div>

END;

    return $result;
}       

public function renderFactureBillet(){
    $billet = \limaga\models\EBillet::find($this->values[0]);

    $idClient = $billet->idClient;
    $num = $this->values[0];
    $client = \limaga\models\Client::find($idClient);
    $nom = $client->nom;
    $prenom = $client->prenom;
    $date = $billet->dateBillet;
    $codeBarre = $billet->codeBarreBillet;

    if($billet->typeAccesBillet == 1){
        $type = "un matin";
    }
    else if($billet->typeAccesBillet == 2){
        $type = "une après-midi";
    }
    else{
        $type = "une journée";
    }

    $result = <<<END

    <div>
        <h4>Billet d'entrée</h4>

        <p>
            N° de e-billet : $num<br/>
            <br/>
            Nom : $nom<br/>
            Prénom : $prenom<br/>
            <br/>
            Billet d'entrée pour $type, le $date<br/>
            Ce billet vous permettra d'obtenir un bracelet grâce à ce code-barre :<br/>
            <br/>
            <b>$codeBarre</b>
        </p>
    </div>

END;
    return $result;
}

public function renderFacturePanier(){
    if(!isset($_SESSION['panier'])){
        $_SESSION['panier'] = array();
    }

    $articles = $_SESSION['panier'];

    $c = \limaga\models\Client::find($this->values[0]);

    $numFacture = rand(0,999999);
    date_default_timezone_set("Europe/Amsterdam");
    $date = date('m/d/Y', time());
    $nom = $c->nom;
    $prenom = $c->prenom;

    $result = <<<END

    <div>

        FACTURE n° : $numFacture <br/>
        <br/>
        Date : $date<br/>
        <br/>
        Nom : $nom&nbsp&nbsp&nbspPrénom : $prenom<br/>
        <br/>
        <br/>
        <i>Prestations facturées</i><br/>

    </div>

END;

    $result .= "</br>
    <table>
        <caption>Articles</caption></br>

        <thead>
         <tr>
            <th>n°</th>
            <th>Prestation</th>
            <th>Prix</th>
        </tr>
    </thead>";

    $numero = 0;

    foreach ($articles as $art) {
        if($art->estLouer){
            $prestation = "location: ".$art->nomProduit;
            $prix = $art->prix;
        }
        else{
            $prestation = "achat: ".$art->nomProduit;
            $prix = $art->prix;
        }

        $result .= "<body>
        <tr>
            <td>$numero</td>
            <td>$prestation</td>
            <td>$prix</td>
        </tr>
        ";

        $numero++;
    }

    return $result;
}


    public function renderMerciBilletLecon(){
        $billet = \limaga\models\EBillet::find($this->values[0]);

        $idClient = $billet->idClient;
        $num = $this->values[0];
        $client = \limaga\models\Client::find($idClient);
        $nom = $client->nom;
        $prenom = $client->prenom;
        $date = $billet->dateBillet;
        $codeBarre = $billet->codeBarreBillet;

        if($billet->typeAccesBillet == 1){
            $type = "un matin";
        }
        else if($billet->typeAccesBillet == 2){
            $type = "une après-midi";
        }
        else{
            $type = "une journée";
        }

        $result = <<<END

        <div>

            <b>Merci pour votre paiement</b><br/><br/>


            N° de e-billet : $num<br/>
            <br/>
            Nom : $nom<br/>
            Prénom : $prenom<br/>
            <br/>
            Billet d'entrée pour $type, le $date<br/>
            Ce billet vous permettra de partiviper à votre lecon  grâce à ce code-barre :<br/>
            <br/>
            <b>$codeBarre</b>
        </p>

    </div>

END;

    return $result;
}       

public function renderFactureBilletLecon(){
    $billet = \limaga\models\EBillet::find($this->values[0]);

    $idClient = $billet->idClient;
    $num = $this->values[0];
    $client = \limaga\models\Client::find($idClient);
    $nom = $client->nom;
    $prenom = $client->prenom;
    $date = $billet->dateBillet;
    $codeBarre = $billet->codeBarreBillet;

    if($billet->typeAccesBillet == 1){
        $type = "un matin";
    }
    else if($billet->typeAccesBillet == 2){
        $type = "une après-midi";
    }
    else{
        $type = "une journée";
    }

    $result = <<<END

    <div>
        <h4>Billet d'entrée</h4>

        <p>
            N° de e-billet : $num<br/>
            <br/>
            Nom : $nom<br/>
            Prénom : $prenom<br/>
            <br/>
            Billet d'entrée pour $type, le $date<br/>
            Ce billet vous permettra de participer à une leçon grâce à ce code-barre :<br/>
            <br/>
            <b>$codeBarre</b>
        </p>
    </div>

END;
    return $result;
}
	
}
