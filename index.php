<?php

require_once 'vendor/autoload.php';

use \limaga\control\VisiteurController;
use \limaga\control\ClientController;
use \limaga\control\FactureController;
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection(parse_ini_file('conf/db.limaga.conf.ini'));

$db->setAsGlobal();
$db->bootEloquent();


$app = new \Slim\Slim();

$app->get('/', function() use($app){
    $vc = new VisiteurController($app->request);
    $vc ->pageaccueil();

}) -> name("base");

$app->get('/deconnexion', function() use($app){
    $cc = new ClientController($app->request);
    $cc -> deconnexion();
}) -> name("deco");

$app->post('/insc', function() use($app){
    $vc = new VisiteurController($app->request);
    $vc->inscrire();
});

$app->get('/insc', function() use ($app){
    $vc = new VisiteurController($app->request);
    $vc -> inscription();
}) -> name("insc");;

$app->post('/auth', function() use ($app){
    $vc = new VisiteurController($app->request);
    $vc -> authentifier();
});

$app->get('/auth', function() use ($app){
    $vc = new VisiteurController($app->request);
    $vc -> auth();
}) -> name("auth");

$app->get('/suppMembre/:id', function($id) use ($app){

    $vc = new ClientController($app->request);
    $vc -> suppMembre($id);

}) -> name("suppMembre");

$app->get('/client/:id', function($id) use ($app){
    $cc = new ClientController($app->request);
    $cc -> afficherClient($id);
}) -> name("client");

$app->get('/admin/:id', function($id) use ($app){
        $cc = new ClientController($app->request);
        $cc -> afficherAdmin($id);
}) -> name("admin");


$app->get('/panier', function() use ($app){
    $vc = new VisiteurController($app->request);
    $vc->afficherPanier();
}) -> name("panier");

$app->get('/addpanier/:idProduit', function($idProduit) use ($app){
    $vc = new VisiteurController($app->request);
    $vc->ajouterProduitPanier($idProduit);
}) -> name("ajouterPanier");

$app->get('/addlouerpanier/:idProduit', function($idProduit) use ($app){
    $vc = new VisiteurController($app->request);
    $vc->ajouterProduitLouerPanier($idProduit);
}) -> name("ajouterLouerPanier");


$app->get('/supprpanier/:idProduit', function($idProduit) use ($app){
    $vc = new VisiteurController($app->request);
    $vc->supprimerProduitPanier($idProduit);
}) -> name("supprimerPanier");

$app->get('/famille', function() use ($app){
    $cc = new ClientController($app->request);
    $cc->ajouterMembre();
}) -> name("famille");

$app->post('/famille', function() use ($app){
    $cc = new ClientController($app->request);
    $cc->insererMembre();
});

$app->get('/catalogue', function() use ($app){
    $vc = new VisiteurController($app->request);
    $vc->afficherCatalogue();
}) -> name("catalogue");

$app->get('/commandeLecon', function() use ($app){
    $cc = new ClientController($app->request);
    $cc->commandeLecon();
}) -> name("commandeLecon");

$app->post('/commandeLecon', function() use ($app){
        $cc = new ClientController($app->request);
        $cc->commanderLecon();
});

$app->post('/leconCollective', function() use ($app){
        $lc = new ClientController($app->request);
        $lc->LeconCollectiveCommandee();
}) -> name("leconCollective");

$app->post('/leconIndividuelle', function() use ($app){
        $li = new ClientController($app->request);
        $li->LeconIndividuelleCommandee();

}) -> name("leconIndividuelle");

$app->post('/caisse', function() use ($app){
        $li = new ClientController($app->request);
        $li->controlleCaisse();

}) -> name("caisse");

$app->get('/affichecaisse', function() use ($app){
        $li = new ClientController($app->request);
        $li->afficheCaisse();

}) -> name("affichecaisse");

$app->get('/billet', function() use ($app){
    $cc = new ClientController($app->request);
    $cc->achatBillet();
})->name('billet');

$app->post('/billet', function() use ($app){
    $cc = new ClientController($app->request);
    $cc->enregistrerBillet();
});

$app->get('/abonnements', function() use ($app){
    $cc = new ClientController($app->request);
    $cc->afficherAbonnements();
}) -> name("abonnements");

$app->get('/eabo', function() use ($app){
    $cc = new ClientController($app->request);
    $cc->souscrireAbo();
})->name("eabo");

$app->get('/eabofamille', function() use ($app){
    $cc = new ClientController($app->request);
    $cc->souscrireAboFamille();
})->name("eabofamille");

$app->get('/choixpaiement/:id', function($id) use ($app){
    $cc = new ClientController($app->request);
    $cc->choixPaiement($id);
})->name('choixpaiement');

$app->get('/choixpaiementLecon/:id', function($id) use ($app){
    $cc = new ClientController($app->request);
    $cc->choixPaiementLecon($id);
})->name('choixpaiementLecon');

$app->get('/mercibillet/:id', function($id) use ($app){
    $fc = new FactureController($app->request);
    $fc->merciBillet($id);
})->name('mercibillet');

$app->get('/impayes/:id', function($id) use ($app){
    $cc = new ClientController($app->request);
    $cc->impayes($id);
})->name('impayes');

$app->get('/facturebillet/:id', function($id) use ($app){
    $fc = new FactureController($app->request);
    $fc->factureBillet($id);
})->name('facturebillet');

$app->get('/mercibilletLecon/:id', function($id) use ($app){
        $fc = new FactureController($app->request);
        $fc->merciBilletLecon($id);
    })->name('mercibilletLecon');

$app->get('/facturebilletLecon/:id', function($id) use ($app){
    $fc = new FactureController($app->request);
    $fc->factureBilletLecon($id);
})->name('facturebilletLecon');


$app->get('/facturepanier/:id', function($id) use ($app){
    $fc = new FactureController($app->request);
    $fc->facturePanier($id);
})->name('facturepanier');

$app->get('/destroy_session', function() use ($app){
    $vc = new VisiteurController($app->request);
    $vc->destroySession();
})->name('ds');

$app->run();

?>
